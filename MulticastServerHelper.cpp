// MulticastServerHelper.cpp : implementation file
//
#include "stdafx.h"
#include "CSocket2Api.h"
#include "Misc.h"
#include "MulticastServerHelper.h"

HANDLE	ghsockMulticast = NULL;
TCHAR	gszGroupIP[32] = "224.0.0.251";
INT		gnGroupPort = 2555;
tEVENTNOTIFY	gteventnotifyMulticastSocket;
CEdit* gpEditEvent = NULL;

BOOL _setup_msocket( HWND _hwnd )
{
	BOOL	fret = TRUE;

	ghsockMulticast = mCreate();

	mSetupNotifyHandle( ghsockMulticast,
		_hwnd, WM_USER+123,
		NULL, NULL,
		(NOTIFYCALLBACK)_proceventnotifyMulticastSocket, &gteventnotifyMulticastSocket );
	mOpen( ghsockMulticast, NULL, gnGroupPort );
	mJoinGroup( ghsockMulticast, gszGroupIP );

	return fret;
}

BOOL _endup_msocket()
{
	if( NULL == ghsockMulticast ) return FALSE;

	BOOL	fret = TRUE;

	mDropGroup( ghsockMulticast );
	mClose( ghsockMulticast );
	mDelete( ghsockMulticast );
	ghsockMulticast = NULL;

	return fret;
}


HRESULT CALLBACK _proceventnotifyMulticastSocket( PVOID _ptData )
{
	HRESULT hr = 0;

	tEVENTNOTIFY*	ptdata = (tEVENTNOTIFY*)_ptData;
	BYTE			acbData[1024*64] = {0,};
	DWORD			dwData = 1024*64;
	
	switch( ptdata->eEvent ) {
	case _FD_READ_READABLE:
		ErrDbg(1, "msock - callback _FD_READ_READABLE \r\n");

		do {			gpEditEvent->LineScroll(gpEditEvent->GetLineCount());
			int len = gpEditEvent->GetWindowTextLength();
			gpEditEvent->SetSel(len,-1,true); 
			gpEditEvent->ReplaceSel((TCHAR*)acbData); 
			hr = mReceivePacketFromQue( ptdata->handle, NULL, &dwData );
			hr = mReceivePacketFromQue( ptdata->handle, acbData, &dwData );
			if( NOERROR != hr ) break;

			if( 0 != memcmp( "EVENT/1.0\n", acbData, 10 ) ) break;

	
		}while(1);
		break;
	}

	return 0;
}