// MulticastServerHelper.h : header file
//

#pragma once

#include "CSocket2Api.h"
/*
	import our function prototype
*/
extern HANDLE	ghsockMulticast;
extern TCHAR	gszGroupIP[32];
extern INT		gnGroupPort;
extern tEVENTNOTIFY	gteventnotifyMulticastSocket;
extern CEdit* gpEditEvent;


BOOL _setup_msocket( HWND _hwnd );
BOOL _endup_msocket();
HRESULT CALLBACK _proceventnotifyMulticastSocket( PVOID _ptData );